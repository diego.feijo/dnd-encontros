new Vue({
      el: '#app',
      data: {
        players:[],
        //Player form
        char_name:"",
        char_initiative:"",
        char_passive_perception:"",
        char_passive_investigation:"",
        char_passive_insight:"",
        //Monsters
        monsters: [],
        dead: "active",
        //Form vars
        mons_name:"",
        dice_num:"",
        hit_die:"",
        base_hp:"",
        dex:"",
        mon_num:"",

      },
      mounted () {
        if (localStorage.getItem('players')) {
          try {
            this.players = JSON.parse(localStorage.getItem('players'));
          } catch (error) {
            localStorage.removeItem('players');
          }
        }
      },
      methods: {
        //Players
        addPlayer () {
          // Garanta que algo foi digitado
          if (!this.char_name &&              
            !this.char_passive_insight &&
            !this.char_passive_investigation &&
            !this.char_passive_perception) 
            return;

            let newPlayer = {
              name: this.char_name,
              initiative: 0,
              insight: this.char_passive_insight,
              investigation: this.char_passive_investigation,
              perception: this.char_passive_perception
            };

          this.players.push(newPlayer);
          this.char_name = '';
          this.char_initiative = '';
          this.char_passive_insight = '';
          this.char_passive_investigation = '';
          this.char_passive_perception = '';
          this.savePlayers();
        },
        removePlayer (index) {
          this.players.splice(index, 1);
          this.savePlayers();
        },
        savePlayers () {
          const parsed = JSON.stringify(this.players);
          localStorage.setItem('players', parsed);
        },
        //monsters
        generate() {          
          let idgen = this.mons_name + this.monsters.length;
          var i;
          for (i = 0; i < this.mon_num; i++) {
            let monstro = {
              id: idgen + i,
              name: this.mons_name + "" + (i + 1),
              hp: ((Math.floor(Math.random() * this.hit_die) + 1) * this.dice_num) + parseInt(this.base_hp),
              initiative: (Math.floor(Math.random() * 20) + 1) + parseInt(this.dex),
              damage: 0,
              acted:false,
              status1: false,
              status2: false,
              status3: false,
              status4: false,
              type: 'monster'
            };
            this.monsters.push(monstro);
          }          
        }
      },
      computed: {
        monsterByInitiative() {
          return this.monsters.slice(0).sort((a, b) => b.initiative - a.initiative);
        },
        turnOrder(){
          let playersAsMonsters = [];
          let idgen = this.monsters.length;
          for(i=0; i<this.players.length; i++){
            let player = this.players[i];
            let pam = {
              id: player.name + idgen + i,
              name: player.name,
              hp: 0,
              initiative: player.initiative,
              damage: 0,
              acted: false,
              status1: false,
              status2: false,
              status3: false,
              status4: false,
              type: 'player'
            };
            playersAsMonsters.push(pam);
          }
          let allofem = this.monsters.concat(playersAsMonsters);
          //Return all of them sorted by initiative
          return allofem.slice(0).sort((a, b) => b.initiative - a.initiative);
        }
      }
    })